//
//  AppDelegate.h
//  PassGen
//
//  Created by Dmitriy Zaporozhets on 12/22/13.
//  Copyright (c) 2013 Dmitriy Zaporozhets. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTextField *PassField;
- (IBAction)generatePass:(id)sender;

@end
