//
//  main.m
//  PassGen
//
//  Created by Dmitriy Zaporozhets on 12/22/13.
//  Copyright (c) 2013 Dmitriy Zaporozhets. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
