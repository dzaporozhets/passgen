//
//  AppDelegate.m
//  PassGen
//
//  Created by Dmitriy Zaporozhets on 12/22/13.
//  Copyright (c) 2013 Dmitriy Zaporozhets. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

- (IBAction)generatePass:(id)sender {
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString *s = [NSMutableString stringWithCapacity:8];
    for (NSUInteger i = 0U; i < 8; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }

    
    [self.PassField setStringValue:s];
}

@end
